export interface TodoItem {
  index: number,
  text: string,
  checked: boolean
}

export interface CreateUser {
  email: string,
  name: string,
  department: string,
  position: string,
  password: string,
  confirmPassword: string
}

export interface User {
  id: string,
  userName: string,
  name: string,
  email: string,
  phoneNumber: string,
  roles: string[],
  department: string,
  position: string,
  projects: string[]
  registeredAt: Date,
  lastUpdateAt: Date,
}

export interface ErrorResponse {
  statusCode: number,
  error?: ServiceError
}

export interface ServiceError {
  code?: string,
  message?: string,
  target?: string
}

export interface LoginResponse {
  succeeded: boolean,
  isLockedOut: boolean,
  isNotAllowed: boolean,
  requiresTwoFactor: boolean
}

export interface Tutorial {
  id: string,
  name: string,
  url: string,
  test: TestItem[],
  departments: Department[],
  positions: Position[],
  projects: Project[],
  createdAt: Date,
  updatedAt: Date,
}

export interface TestItem {
  question: string,
  option0: string,
  option1: string,
  option2: string,
  answer: number,
}

export interface Position {
  id: string,
  name: string,
  departament: string,
  createdAt: Date,
  updateAt: Date
}

export interface Department {
  id: string,
  name: string,
  createdAt: Date,
  updateAt: Date
}

export interface Project {
  id: string,
  name: string,
  departament: string,
  createdAt: Date,
  updateAt: Date
}

export interface UserTutorial {
  id: string,
  name: string,
  url: string,
  test: TestItem[],
  createdAt: Date,
  updatedAt: Date,
  completed: boolean,
}