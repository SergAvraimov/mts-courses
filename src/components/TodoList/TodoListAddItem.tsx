import React, { useState } from "react";
import { createUseStyles } from "react-jss";
import { ReactComponent as AddIcon } from "../../assets/svg/add-24px.svg";
import { ReactComponent as SendIcon } from "../../assets/svg/send-24px.svg";
import clsx from "clsx";

interface TodoAddItemProps {
  addItem: (text: string) => void;
}

export function TodoListAddItem(props: TodoAddItemProps) {
  const classes = useStyles();
  const [text, setText] = useState("");
  let textInput: HTMLInputElement | null;

  const onChangeValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  };

  const onClickAddItem = () => {
    if (!!text) {
      props.addItem(text);
      setText("");
    }
  };

  const onClickAddIcon = () => {
    textInput?.focus();
  };

  const onKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      onClickAddItem();
    }
  };

  return (
    <div className={classes.item}>
      <button className={classes.addIcon} onClick={onClickAddIcon}>
        <AddIcon />
      </button>
      <input
        type="text"
        value={text}
        className={classes.itemText}
        onChange={onChangeValue}
        onKeyPress={onKeyPress}
        placeholder="Add todo"
        ref={input => {
          textInput = input;
        }}
      />
      <button className={clsx(classes.addItemButton, { [classes.collapse]: !text })} onClick={onClickAddItem}>
        <SendIcon fill="#303030" />
      </button>
    </div>
  );
}

const useStyles = createUseStyles({
  item: {
    display: "flex"
  },
  addIcon: {
    width: "24px",
    height: "24px",
    margin: "8px 10px 8px 20px",
    cursor: "pointer",
    padding: 0,
    border: 0,
    background: "transparent",

    "&:focus": {
      outline: "1px solid #c0c0c0"
    }
  },
  itemText: {
    flex: 1,
    borderWidth: 0,
    backgroundColor: "transparent",
    padding: "5px",
    margin: "5px 0px",

    "&:focus": {
      outline: "1px solid #c0c0c0"
    }
  },
  collapse: {
    visibility: "collapse"
  },
  addItemButton: {
    cursor: "pointer",
    margin: "5px",
    marginRight: "15px",
    color: "#909090",
    border: 0,
    background: "transparent",

    "&:focus": {
      outline: "1px solid #c0c0c0"
    }
  }
});
