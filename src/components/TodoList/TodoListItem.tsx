import React from "react";
import { TodoItem } from "../../types";
import { createUseStyles } from "react-jss";
import { ReactComponent as CheckBoxCheckedIcon } from "../../assets/svg/check_box-24px.svg";
import { ReactComponent as CheckBoxBlankIcon } from "../../assets/svg/check_box_outline_blank-24px.svg";
import clsx from "clsx";

interface TodoItemProps {
  item: TodoItem;
  checkItem: (index: number) => void;
  removeItem: (index: number) => void;
  changeText: (index: number, text: string) => void;
}

export function TodoListItem({ item, ...props }: TodoItemProps) {
  const classes = useStyles();

  const onClickCheck = () => {
    props.checkItem(item.index);
  };

  const onClickDelete = (event: any) => { // баг в типах с blur, поэтому any
    event.target.blur();
    props.removeItem(item.index);
  };

  const onChangeValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.changeText(item.index, event.target.value);
  };

  return (
    <div className={clsx(classes.item)}>
      <button className={classes.checkButton} onClick={onClickCheck}>
        {item.checked ? <CheckBoxCheckedIcon fill="#909090" /> : <CheckBoxBlankIcon fill="#000000" />}
      </button>

      <input
        type="text"
        value={item.text}
        className={clsx(classes.itemText, { [classes.chacked]: item.checked })}
        onChange={onChangeValue}
      />
      <button className={classes.removeItem} onClick={onClickDelete}>
        ✕
      </button>
    </div>
  );
}

const useStyles = createUseStyles({
  item: {
    display: "flex"
  },
  checkButton: {
    width: "24px",
    height: "24px",
    margin: "8px 10px 8px 20px",
    padding: 0,
    border: 0,
    background: "transparent",
    cursor: "pointer",

    "&:focus": {
      outline: "1px solid #c0c0c0"
    }
  },
  chacked: {
    color: "#c0c0c0",
    textDecoration: "line-through"
  },
  itemText: {
    flex: 1,
    borderWidth: 0,
    background: "transparent",
    padding: "5px",
    margin: "5px 0px",
    fontSize: "18px",

    "&:focus": {
      outline: "1px solid #c0c0c0"
    }
  },
  removeItem: {
    cursor: "pointer",
    margin: "5px",
    marginRight: "20px",
    color: "#909090",
    border: 0,
    background: "transparent",
    fontSize: "20px",

    "&:focus": {
      outline: "1px solid #c0c0c0"
    }
  }
});
