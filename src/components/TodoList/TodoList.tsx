import React from "react";
import { TodoItem } from "../../types";
import { TodoListItem } from "./TodoListItem";
import './TodoList.scss';
import { useLocalStorage } from "../../hooks/useLocalStorage";
import { TodoListAddItem } from "./TodoListAddItem";

export function TodoList() {
  const [todos, setTodos] = useLocalStorage<TodoItem[]>("data", []);

  const removeItem = (index: number) => {
    const filteredTodos = todos.filter(_ => _.index !== index);
    setTodos(filteredTodos);
  };

  const checkItem = (index: number) => {
    setTodos(
      todos.map(_ => {
        if (_.index === index) _.checked = !_.checked;
        return _;
      })
    );
  };

  const addItem = (text: string) => {
    let maxIndex =
      todos.length > 0
        ? Math.max.apply(
            null,
            todos.map(_ => _.index)
          )
        : 0;
    const newTodo = {
      index: maxIndex + 1,
      text,
      checked: false
    };

    setTodos([...todos, newTodo]);
  };

  const changeText = (index: number, text: string) => {
    setTodos(
      todos.map(_ => {
        if (_.index === index) _.text = text;
        return _;
      })
    );
  };

  const todosSorted = todos.sort((a, b) => {
    if (a.checked > b.checked) {
      return 1;
    }
    if (a.checked < b.checked) {
      return -1;
    }

    if (a.index > b.index) {
      return -1;
    }
    if (a.index < b.index) {
      return 1;
    }

    return 0;
  });

  const todoItems = todosSorted.map(item => (
    <li key={item.index} className={"todoListItem"}>
      <TodoListItem
        item={item}
        removeItem={removeItem}
        checkItem={checkItem}
        changeText={changeText}
      />
    </li>
  ));

  return (
    <ul className={"todoList"}>
      <li className={"todoListItem"}>
        <TodoListAddItem addItem={addItem} />
      </li>
      {todoItems}
    </ul>
  );
}
