import React, { Component } from 'react'
import {
  Container,
  Row,
  Col,
  Button
} from 'react-bootstrap';
import egg from '../../assets/svg/egg-1.svg';
import arrowr from '../../assets/svg/arrow-r-light.svg';
import { useAuth } from '../../providers/AuthProvider';


export default function Sidebar() {
  const [isLogged, user] = useAuth();
  return (
    <>
      {isLogged && <aside className='sidebar'>
        <Container>
          <Row>
            <Col>
              <div className="employee">
                <div className="employee__img img">
                  <div className="img__cover img__cover--rounded">
                    <img
                      src="https://sun9-4.userapi.com/c824201/v824201969/173426/YW0DIgHPsvw.jpg?ava=1"
                      alt="employee img"
                    />
                  </div>
                </div>
                <div className="employee__info">
                  <h3 className="employee__name">{user?.name}</h3>
                  <h4 className="employee__position">{user?.department + " " + user?.position}</h4>
                </div>
              </div>
              <div className="egg text-center">
                <img src={egg} alt="" />
              </div>
              <div className="egg__next-level text-center">
                <div className="egg__divider"></div>
                <div className="egg-item">
                  <div className="egg-item__img img">
                    <div className="img__cover img__cover--rounded">
                      <img
                        src="https://sun9-4.userapi.com/c824201/v824201969/173426/YW0DIgHPsvw.jpg?ava=1"
                        alt="egg img"
                      />
                    </div>
                  </div>
                </div>
                <div className="egg__continue">
                  <p><span>next level</span></p>
                  <Button href="/task" variant="link"><span>Перейти</span><img src={arrowr} alt="arrow" /></Button>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </aside>}
    </>
  )
}
