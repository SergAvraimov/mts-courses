import React from 'react'
import {
	Navbar,
	Container,
	Nav
} from 'react-bootstrap';

import logo from '../../assets/svg/MTS_logo.svg';
import NavbarToggle from 'react-bootstrap/NavbarToggle';
import NavbarCollapse from 'react-bootstrap/NavbarCollapse';
import { useAuth, logout } from '../../providers/AuthProvider';
import { useLocation } from 'react-router-dom';

export default function Header() {
	const [isLogged, user] = useAuth();
	const location = useLocation();
	return (
		<>
			<Navbar collapseOnSelect expand="md">
				<Container>
					<Navbar.Brand href="/">
						<img
							src={logo}
							width="50"
							className="d-inline-block align-middle"
							alt="logo"
						/>
					</Navbar.Brand>
					<NavbarToggle aria-controls="responsive-navbar-nav" />
					<NavbarCollapse id="responsive-navbar-nav">
						<Nav className="d-flex w-100 mr-auto pt-2 mb-2 pt-md-0 mb-md-0" activeKey={location.pathname}>
							<Nav.Link href="/">Задания</Nav.Link>
							<Nav.Link href="#">Прогресс</Nav.Link>
							<Nav.Link href="#">Уведомления<span>2</span></Nav.Link>
							<div className="d-flex ml-auto mr-0">
								{!isLogged && <Nav.Link href="/login">Войти</Nav.Link>}
								{!isLogged && <Nav.Link href="/signup">Зарегестрироваться</Nav.Link>}
								{isLogged && <Nav.Link href="/login" onClick={() => logout()}>Выйти</Nav.Link>}
							</div>
						</Nav>
					</NavbarCollapse>

				</Container>
			</Navbar>
		</>
	)
}
