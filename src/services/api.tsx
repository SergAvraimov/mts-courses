import { Consts } from "../Consts";
import { User, CreateUser, ErrorResponse, LoginResponse, Tutorial, UserTutorial } from "../types";
import { TodoList } from "../components/TodoList/TodoList";
import { Notify } from "./Notify";

export class Api {

  public static async getDepartaments(): Promise<string[]> {
    return await Api.get<string[]>(`${Consts.ApiUrl}/departments`);
  }

  public static async getPositions(department: string): Promise<string[]> {
    return await Api.post<string[]>(`${Consts.ApiUrl}/positions/all`, { department });
  }

  public static async createUser(user: CreateUser): Promise<User> {
    return await Api.post<User>(`${Consts.ApiUrl}/users`, user);
  }

  public static async getUserInfo(showError = false): Promise<User> {
    const a = await Api.get<any>(`${Consts.ApiUrl}/users/info`, showError);
    return a;
  }

  public static async getTutorials(): Promise<UserTutorial[]> {
    return await Api.get<any>(`${Consts.ApiUrl}/tutorials`);
  }

  public static async getTutorial(id: string): Promise<Tutorial> {
    return await Api.get<Tutorial>(`${Consts.ApiUrl}/tutorials/${id}`);
  }

  public static async login(email: string, password: string): Promise<LoginResponse> {
    return await Api.post<LoginResponse>(`${Consts.ApiUrl}/auth/login`, { email, password });
  }

  public static async logout() {
    return await Api.post<any>(`${Consts.ApiUrl}/auth/logout`);
  }

  public static async tutorialComplete(id: string) {
    return await Api.patch<any>(`${Consts.ApiUrl}/tutorials/${id}`);
  }

  private static async get<T>(request: string, showError = true): Promise<T> {
    try {
      const res = await (await fetch(request, {
        credentials: "include",
        mode: 'cors',
      })).json() as T | ErrorResponse;
      if (!res || !!(res as ErrorResponse)?.error?.message) {
        if (showError)
          Notify.error((res as ErrorResponse)?.error?.message as string);
      }
      else
        return res as T;
    } catch (error) {
      console.log(error);
    }
    return {} as T;
  }

  private static async post<T>(request: string, payment?: object, showError = true): Promise<T> {
    return Api.sendRequest<T>(request, payment, showError, "POST");
  }

  private static async patch<T>(request: string, payment?: object, showError = true): Promise<T> {
    return Api.sendRequest<T>(request, payment, showError, "PATCH");
  }

  private static async sendRequest<T>(request: string, payment?: object, showError = true, method: string = "POST"): Promise<T> {
    try {
      const req = new Request(request, {
        method: method,
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "include",
        mode: 'cors',
        body: JSON.stringify(payment)
      });
      const res = await (await fetch(req)).json() as T | ErrorResponse;
      if (!res || !!(res as ErrorResponse)?.error?.message) {
        if (showError)
          Notify.error((res as ErrorResponse)?.error?.message as string);
      }
      else
        return res as T;
    } catch (error) {
      console.log(error);
    }
    return {} as T;
  }
}