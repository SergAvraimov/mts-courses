import React from 'react';
import './App.scss';
import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import { Tasks } from './pages/Tasks';
import { Task } from './pages/Task';
import { Users } from './pages/Users';
import { Login } from './pages/Login';
import { Signup } from './pages/SignUp';
import { Test } from './pages/Test';
import { useAuth } from './providers/AuthProvider';

function App() {
  const [isLogged, user] = useAuth();
  return (
    <>
      <div className="layout">
        <Sidebar />
        <div className="content-wrap">
          <BrowserRouter>
            <Header />
            <div className="content">
              <Switch>
                <Route exact path="/" component={Tasks} />
                <Route path="/task/:id" render={(props) => <Task taskId={props.match.params.id as string || "0"} />} />
                <Route path="/users" component={Users} />
                <Route path="/test/:id" render={(props) => <Test taskId={props.match.params.id as string || "0"} />} />
                <Route path="/login" component={Login} />
                <Route path="/signup" component={Signup} />
                <Route path="*" render={({ location }) => (
                  <Redirect to={{ pathname: "/", state: { from: location } }} />
                )} />
              </Switch>

            </div>
            <ToastContainer position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover />
          </BrowserRouter>
        </div>
      </div>
    </>
  );
}

export default App;



