import React, { useEffect, useState } from 'react';
import { Tabs, Tab, Container, Row, Col, Button, ListGroup } from 'react-bootstrap';
import arrowr from '../assets/svg/arrow-r.svg';
import { Api } from '../services/api';
import { utils } from '../services/utils';
import { UserTutorial } from '../types';
import { useAuth } from '../providers/AuthProvider';

export function Tasks() {
  const [loggedIn, user] = useAuth();
  const [tutorials, setTutorials] = useState<UserTutorial[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const res = await Api.getTutorials();
      if (utils.isEmptyObject(res))
        setTutorials([]);
      else
        setTutorials(res);
    };
    fetchData();
  }, []);

  const getTutorialView = (tutorial: UserTutorial) => {
    const date = new Date(tutorial.createdAt)
    date.setDate(date.getDate() + 7)
    return (
      <ListGroup.Item className="task-item" active>
        <span className="task-item__name">{tutorial.name} </span>
        <span className="task-item__date">до {date.toISOString().slice(0,10)}</span>
        <Button href={`/task/${tutorial.id}`} variant="link"><span>Перейти</span><img src={arrowr} alt="arrow" /></Button>
      </ListGroup.Item>);
  }

  // const positionTutorials = tutorials?.filter(x => x.positions.map(y => y.name).includes(user.position));
  // const departmentTutorials = tutorials?.filter(x => x.departments.map(y => y.name).includes(user.department));
  // const projectTutorials = tutorials?.filter(x => user.projects.some(p =>  x.departments.map(d => d.name).includes(p)));

  return (
    <Container>
      {loggedIn && <Row>
        <Col xs="12">
          <Tabs className="tasks-tabs" defaultActiveKey="all" id="tasks-tabs">
            <Tab eventKey="all" title="Все">
              <ListGroup>
                {tutorials.map(x => getTutorialView(x))}
                <ListGroup.Item className="task-item passed">
                  <span className="task-item__name">1. Задание первого дня</span>
                  <span className="task-item__date">до 10.05.2020</span>
                  <Button href="/task/0" variant="link"><span>Перейти</span><img src={arrowr} alt="arrow" /></Button>
                </ListGroup.Item>
                <ListGroup.Item className="task-item" active>
                  <span className="task-item__name">2. Задание 2 дня</span>
                  <Button href="/task/0" variant="link"><span>Перейти</span><img src={arrowr} alt="arrow" /></Button>
                </ListGroup.Item>
                <ListGroup.Item className="task-item">
                  <span className="task-item__name">3. Задание 3 дня</span>
                  <Button href="/task/0" variant="link"><span>Перейти</span><img src={arrowr} alt="arrow" /></Button>
                </ListGroup.Item>
                <ListGroup.Item className="task-item">
                  <span className="task-item__name">4. Задание 4 дня</span>
                  <Button href="/task/0" variant="link"><span>Перейти</span><img src={arrowr} alt="arrow" /></Button>
                </ListGroup.Item>
              </ListGroup>
            </Tab>
            <Tab eventKey="department" title="Отдел">
              <ListGroup>
                {tutorials.map(x => getTutorialView(x))}
              </ListGroup>
            </Tab>
            <Tab eventKey="position" title="Должность">
              <ListGroup>
                {tutorials.map(x => getTutorialView(x))}
              </ListGroup>
            </Tab>
            <Tab eventKey="projects" title="Проекты">
              <ListGroup>
                {tutorials.map(x => getTutorialView(x))}
              </ListGroup>
            </Tab>
          </Tabs>
        </Col>
      </Row>}
    </Container>
  );
}