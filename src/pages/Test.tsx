import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import arrowl from '../assets/svg/arrow-l.svg';
import { Tutorial } from '../types';
import { Api } from '../services/api';
import { utils } from '../services/utils';
import { Notify } from '../services/Notify';
import { useHistory } from 'react-router-dom';

export function Test(props: { taskId: string }) {
    const [task, setTask] = useState<Tutorial | null>(null);
    const history = useHistory();

    if (!task && props.taskId === "0") {
        const testTask: Tutorial = {
            id: "0",
            name: "Тестовое задание",
            url: "https://www.figma.com/file/Fd33iqih1WtDzwzYYBQjX0/Untitled?node-id=5%3A23530",
            test: [{
                question: "Какой-то вопрос на 2 строки по подходящей теме в узкой специальности, который связан с материалом, который находился в ссылке",
                option0: "Какой-либо ответ 1 с материалом из ссылки",
                option1: "Какой-либо ответ 2 с материалом из ссылки",
                option2: "Какой-либо ответ 3 с материалом из ссылки",
                answer: 1,
            }],
            departments: [],
            positions: [],
            projects: [],
            createdAt: new Date(),
            updatedAt: new Date(),
        }
        setTask(testTask);
    }

    useEffect(() => {
        const fetchData = async () => {
            if (!props.taskId || props.taskId === "0") return;

            const res = await Api.getTutorial(props.taskId);
            if (utils.isEmptyObject(res))
                setTask(null);
            else
                setTask(res);
        };
        fetchData();
    }, []);

    async function handleAnswer(event: any) {
        event.preventDefault();
        Notify.success("Тест успешно пройден");
        if(!!task)
            await Api.tutorialComplete(task.id);
        history.push("/");
    }

    return (
        <Container>
            <Row className="task-single">
                <Col xs="12">
                    <Row className="task-single__info">
                        <Col xs="12">
                            <Button href="../" className="back" variant="link"><img src={arrowl} alt="arrow" /><span>Назад</span></Button>
                        </Col>
                        <Col xs md="auto">
                            <h1 className="task-single__title">ТЕСТ</h1>
                        </Col>
                        <Col className="text-right">
                            <span className="task-single__date">1 / 10</span>
                        </Col>
                        <Col xs="12" className="task-single__desc task-single__test">
                            <Form onSubmit={handleAnswer}>
                                <Form.Group as={Row}>
                                    <Col xs="12">
                                        <p>{task?.test[0]?.question}</p>
                                    </Col>
                                    <Col xs="12">
                                        <Form.Check
                                            type="radio"
                                            label={task?.test[0]?.option0}
                                            name="formHorizontalRadios"
                                            id="formHorizontalRadios1"
                                        />
                                        <Form.Check
                                            type="radio"
                                            label={task?.test[0]?.option1}
                                            name="formHorizontalRadios"
                                            id="formHorizontalRadios2"
                                        />
                                        <Form.Check
                                            type="radio"
                                            label={task?.test[0]?.option2}
                                            name="formHorizontalRadios"
                                            id="formHorizontalRadios3"
                                        />
                                    </Col>
                                </Form.Group>
                                <Form.Group as={Row}>
                                    <Col xs="12" className="text-center">
                                        <Button type="submit">ОТВЕТИТЬ</Button>
                                    </Col>
                                </Form.Group>
                            </Form>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );
}
