import React, { useState, useEffect } from "react";
import {
  Button,
  FormGroup,
  FormControl,
  FormLabel
} from "react-bootstrap";
import { Api } from "../services/api";
import { User, CreateUser, ErrorResponse } from "../types";
import { toast } from 'react-toastify';
import { Notify } from "../services/Notify";
import { utils } from "../services/utils";
import { useLocation, useHistory } from "react-router-dom";

export function Signup() {
  const [user, setUser] = useState<CreateUser>({
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
    position: "",
    department: "",
  });
  const [departments, setDpartments] = useState<string[]>([]);
  const [positions, setPositions] = useState<string[]>([]);
  const history = useHistory();

  function validateForm() {
    return user.email.length > 0 && user.password.length > 0;
  }

  async function handleSubmit(event: any) {
    event.preventDefault();
    var resUser = await Api.createUser(user)
    utils.isEmptyObject(resUser);
    if (!utils.isEmptyObject(resUser)) {
      history.push("/login");
    }
  }

  useEffect(() => {
    const fetchData = async () => {
      const res = await Api.getDepartaments();
      if (utils.isEmptyObject(res))
        setDpartments([]);
      else
        setDpartments(res);
    };
    fetchData();
  }, []);

  function handleDepartamentChange(value: any) {
    setUser({ ...user, department: value })
    const fetchData = async () => {
      const res = await Api.getPositions(value);
      if (utils.isEmptyObject(res))
        setPositions([]);
      else
        setPositions(res);
    };
    fetchData();
  }

  return (
    <div className="login">
      <div className="login-form text-center">
        <h3>РЕГИСТРАЦИЯ НА СЕРВИСЕ</h3>
        <form onSubmit={handleSubmit}>
          <FormGroup>
            <FormControl
              type="text"
              placeholder="Введите ФИО"
              value={user.name}
              onChange={e => setUser({ ...user, name: e.target.value })}
            />
          </FormGroup>
          <FormGroup controlId="email">
            <FormControl
              autoFocus
              type="email"
              value={user.email}
              placeholder="Введите почту"
              onChange={e => setUser({ ...user, email: e.target.value })}
            />
          </FormGroup>
          <FormGroup controlId="password">
            <FormControl
              type="password"
              value={user.password}
              placeholder="Введите пароль"
              onChange={e => setUser({ ...user, password: e.target.value })}
            />
          </FormGroup>
          <FormGroup controlId="confirmPassword">
            <FormControl
              type="password"
              value={user.confirmPassword}
              placeholder="Подтвердите пароль"
              onChange={e => setUser({ ...user, confirmPassword: e.target.value })}
            />
          </FormGroup>
          <FormGroup controlId="departments">
            <FormControl as="select"
              onChange={e => handleDepartamentChange(e.target.value)}
              value={user.department}>
              <option></option>
              {departments?.map(x => <option>{x}</option>)}
            </FormControl>
          </FormGroup>
          <FormGroup controlId="positions">
            <FormControl as="select"
              onChange={e => setUser({ ...user, position: e.target.value })}
              value={user.position}>
              <option></option>
              {positions?.map(x => <option>{x}</option>)}
            </FormControl>
          </FormGroup>
          <Button disabled={!validateForm()} type="submit">
            ЗАРЕГИСТРИРОВАТЬСЯ
          </Button>
        </form>
      </div>
    </div>
  );
}