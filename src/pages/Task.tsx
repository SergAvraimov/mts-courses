import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import arrowl from '../assets/svg/arrow-l.svg';
import illustration from '../assets/svg/Illustration.svg';
import { Tutorial } from '../types';
import { Api } from '../services/api';
import { utils } from '../services/utils';

export function Task(props: { taskId: string }) {
  const [task, setTask] = useState<Tutorial | null>(null);
  // const taskId = "0";
  if (!task && props.taskId === "0") {
    const testTask: Tutorial = {
      id: "0",
      name: "Тестовое задание",
      url: "https://www.figma.com/file/Fd33iqih1WtDzwzYYBQjX0/Untitled?node-id=5%3A23530",
      test: [],
      departments: [],
      positions: [],
      projects: [],
      createdAt: new Date(),
      updatedAt: new Date(),
    }
    setTask(testTask);
  }

  useEffect(() => {
    const fetchData = async () => {
      if (!props.taskId || props.taskId === "0") return;

      const res = await Api.getTutorial(props.taskId);
      if (utils.isEmptyObject(res))
        setTask(null);
      else
        setTask(res);
    };
    fetchData();
  }, []);

  let endDate: Date | null = null;

  if (!!task) {
    const eDate = new Date(task.createdAt);
    eDate.setDate(eDate.getDate() + 7)
    endDate = eDate;
  }

  return (
    <Container>
      {!!task && <Row className="task-single">
        <Col xs="12" lg="6">
          <Row className="task-single__info">
            <Col xs="12">
              <Button href="../" className="back" variant="link"><img src={arrowl} alt="arrow" /><span>Назад</span></Button>
            </Col>
            <Col xs md="auto">
              <h1 className="task-single__title">{task.name}</h1>
            </Col>
            {!!endDate && <Col className="text-right" >
              <span className="task-single__date">до {(endDate as Date).toISOString().slice(0, 10)}</span>
            </Col>}
            <Col xs="12" className="task-single__desc">
              <p>Прямо сейчас прочитай материалы по ссылке ниже. <br />В них описаные очень важные штуки, без знаний <br />о который у тебя могут возникать трудности. <br />Давай избежим этого прямо сейчас</p>
            </Col>
            <Col xs="12">
              <Button
                href={task.url}
                className="task-single__link">
                {task.url}
              </Button>
            </Col>
          </Row>
        </Col>
        <Col xs="12" lg="6" className="task-single__img">
          <img src={illustration} alt="illustration" />
        </Col>
        <Col xs="12" className="task-single__test text-center">
          <Button href={"/test/" + task.id}>ПЕРЕЙТИ К ТЕСТУ</Button>
        </Col>
      </Row>}
    </Container>
  );
}
