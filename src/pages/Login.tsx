import React, { useState } from 'react';
import {
  Button,
  FormGroup,
  FormControl,
  FormLabel
} from 'react-bootstrap';
import { login } from '../providers/AuthProvider';
import { useHistory } from 'react-router-dom';
import { Api } from '../services/api';
import { utils } from '../services/utils';
import { Notify } from '../services/Notify';

export function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  async function handleSubmit(event: any) {
    event.preventDefault();
    const res = await Api.login(email, password);
    if (!utils.isEmptyObject(res)) {
      if (res.succeeded) {
        const userInfo = await Api.getUserInfo();
        if (!utils.isEmptyObject(userInfo)) {
          login(userInfo);
          history.push("/");
          return;
        }
      }
      Notify.error("Вход не выполнен!");
    }
  }

  return (
    <div className="login">
      <div className="login-form text-center">
        <h3>АВТОРИЗАЦИЯ</h3>
        <form onSubmit={handleSubmit}>
          <FormGroup controlId="email">
            <FormControl
              autoFocus
              type="email"
              value={email}
              placeholder="Введите почту"
              onChange={e => setEmail(e.target.value)}
            />
          </FormGroup>
          <FormGroup controlId="password">
            <FormControl
              value={password}
              onChange={e => setPassword(e.target.value)}
              placeholder="Введите пароль"
              type="password"
            />
          </FormGroup>
          <Button disabled={!validateForm()} type="submit">
            ВОЙТИ
        </Button>
        </form>
      </div>
    </div>
  );
}