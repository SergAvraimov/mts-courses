import { TokenProvider } from "./TokenProvider";
import { useState, useEffect } from "react";
import { User } from "../types";
import { Api } from "../services/api";
import { utils } from "../services/utils";

export const AuthProvider = () => {
  const tokenProvider = TokenProvider();

  const login: typeof tokenProvider.setUser = (user) => {
    tokenProvider.setUser(user);
  };

  const logout = async () => {
    await Api.logout();
    clearCookie(".AspNetCore.Identity.Application", "localhost", "/");
    clearCookie(".AspNetCore.Identity.Application", "lapkisoft.me", "/");
    tokenProvider.setUser(null);
  };

  const clearCookie = (name: string, domain: string, path: string) => {
      var domain = domain || document.domain;
      var path = path || "/";
      document.cookie = name + "=; expires=" + +((new Date).getDate() - 1) + "; domain=" + domain + "; path=" + path;
  };

  const useAuth = () => {
    const [user, setUser] = useState<User | null>(null);
    const [isLogged, setIsLogged] = useState(tokenProvider.isLoggedIn());

    useEffect(() => {
      const listener = (newIsLogged: boolean | null) => {
        const fetchData = async () => {
          await tokenProvider.fetchUser();
          const resUser = getUser();
          if (!utils.isEmptyObject(resUser)) {
            setUser(getUser())
            setIsLogged(true);
          } else {
            setUser(null)
            setIsLogged(false);
          }
        }
        fetchData();
      };
      listener(null);
      tokenProvider.subscribe(listener);
      return () => {
        tokenProvider.unsubscribe(listener);
      };
    }, []);

    return [isLogged, user] as [typeof isLogged, User];
  };

  const getUser = (): User | null => {
    return tokenProvider.getUser();
  }

  return {
    useAuth,
    login,
    logout
  }
};

export const { useAuth, login, logout } = AuthProvider();