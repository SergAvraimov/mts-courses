import { User } from "../types";
import { Api } from "../services/api";
import { utils } from "../services/utils";

export const TokenProvider = () => {
  let _user: User | null;

  const getUser = () => {
    if (!_user) {
      return null;
    }

    return _user;
  };

  const setUser = (user: User | null) => {
    _user = user;
    notify();
  };

  const fetchUser = async () => {
    if (!_user) {
      _user = await Api.getUserInfo(false);
      if (utils.isEmptyObject(_user))
        _user = null;
    }
  }

  const isLoggedIn = () => {
    return !!_user;
  };

  let observers: Array<(isLogged: boolean) => void> = [];

  const subscribe = (observer: (isLogged: boolean) => void) => {
    observers.push(observer);
  };

  const unsubscribe = (observer: (isLogged: boolean) => void) => {
    observers = observers.filter(_observer => _observer !== observer);
  };

  const notify = () => {
    const isLogged = isLoggedIn();
    observers.forEach(observer => observer(isLogged));
  };

  return {
    getUser,
    isLoggedIn,
    setUser,
    subscribe,
    unsubscribe,
    fetchUser
  };
};

export const isAuthenticated = (user: User) => !!user;



